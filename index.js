console.log("Hello World!");

// function printInfo() {
// 	let nickName = prompt ("Enter your nickname: ");
// 	console.log("Hi, " + nickName);
// };

// printInfo(); //invoke
				// parameter
function printName(firstName) {
	console.log("My name is " + firstName);
};

printName("Juana"); // argument
printName("John");
printName("Cena");

// Now we have a reusable function/reusable task but could have different output based on what value to process, with the help of...
// [SECTION] Parameters and Arguments

// Parameter
	// "firstName" is called a parameter
	// A "parameter" acts as a name variable/container that exists only inside a function
	// It is used to store information that is provided to a function when it is called/invoked

// Argument
	// "Juana", "John", and "Cena" the information/data provided directly into the function is called "argument".
	// values passed when invoking a function are called arguments
	// These arguments are then stored as the parameter within the function

let sampleVariable = "Yui";

printName(sampleVariable);
// Variable can also be passed as an argument

// ------------------------------------------------

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibilityBy8 = remainder === 0; //will store a value true/false
	console.log("Is " + num + " divisible by 8?");	
	console.log(isDivisibilityBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [SECTION] Function as argument
	// Function parameters can also accept functions as arguments
	// Some complex functions uses other functions to perform more complicated results.

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction) {
		argumentFunction();
	};

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

// -----------------------------------------------------

// [SECTION] Using Multiple Parameters

function createFullName(firstName, middleName, lastName) {
	console.log("My full name is " + firstName + " " + middleName + " " + lastName);
};

// Using variables as an argument
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);


createFullName("Christopher", "Katigbak", "Malinao");
createFullName("Katigbak", "Christopher", "Malinao");

function getDifferenceof8Minus4(numA, numB) {
	console.log("Difference: " + (numA - numB));
};

getDifferenceof8Minus4(8, 4);
getDifferenceof8Minus4(4, 8); // This will result to logical error

// [SECTION] Return statement

	function returnFullName(firstName, middleName, lastName) {
		// return firstName + " " + middleName + " " + lastName;



		// We could also create a variable inside the function to contain the result and return the variable instead
		let fullName = firstName + " " + middleName + " " + lastName;
		return fullName;

		// this line of code will not be printed
		console.log("This is printed inside a function");
	};

	let completeName = returnFullName("Jeffrey", "Smith", "Jordan");
	console.log(completeName);

	console.log("I am " + completeName);

	function printPlayerInfo(username, level, job) {
		console.log("Username: " + username)
		console.log("Level: " + level)
		console.log("Job: " + job)
	}

	let user1 = printPlayerInfo("boxzMapagmahal", "Senior", "Programmer");
	console.log(user1); // returns undefined